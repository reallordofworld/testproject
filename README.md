# CREW APPLICATION

Simple application which represents dashboard with candidates.

# CREW file structure

Tests are located in /cypress/integration/tests folder
Used TestObject (Components files and locators.js file )
Web App Functionality separated on components files located in /cypress/integration/components folder

### Running via Cypress Framework
1. Install Cypress framework 
```shell
npm install cypress --save-dev
```
2. Install Docker
3. Build Docker
4. Run Docker
5. Run Cypress, using command: ``npm run cypress:open``

### Tests located in Cypress/integration/tests

### Running locally
`yarn install`

`yarn start`

App will be available on http://localhost:3000


### Running in docker
`docker build -t crew-app .`

`docker run -it --rm -p 5000:5000 --name crew-container crew-app`

App will be available on http://localhost:5000