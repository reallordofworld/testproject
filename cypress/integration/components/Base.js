import locators from '../locators';

// class for common functionality
// all components are extends BaseComponent
class BaseComponent {
    constructor() {
        this.componentId = this.getComponentId();
        this.$$ = this.getLocators();
    }

    // returned string which need to pull locators to this.$$
    getComponentId() {
        return '';
    }

    // get locators by componentId from 'cypress/integration/locators.js'
    getLocators() {
        return locators[this.componentId];
    }
}

export default BaseComponent
