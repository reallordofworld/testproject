import BaseComponent from './Base'

class BoardComponent extends BaseComponent {
    // returned string which need to pull locators to this.$$
    getComponentId() {
        return 'board';
    }

    // check that board contains member name
    isMemberExists(memberName) {
        this.getMemberName().contains(memberName);

        return this;
    }

    // check that board doesn`t contain member name
    isMemberNotExist(memberName) {
        this.getMemberName().should('have.not.text', memberName);
        return this;
    }

    getMemberName() {
        return cy.get(this.$$.memberName);
    }
}

export default BoardComponent
