import BaseComponent from './Base';

class SearchComponent extends BaseComponent {
    // returned string which need to pull locators to this.$$
    getComponentId() {
        return 'search';
    }

    clickSubmit() {
        this.getSubmitButton().click();
        return this;
    }

    clickClear() {
        this.getClearButton().click();
        return this;
    }

    enterName(value) {
        this.getNameField().type(value);
        return this;
    }

    nameFieldContains(value) {
        this.getNameField().should('have.value', value);
        return this;
    }

    enterCity(value) {
        this.getCityField().type(value);
        return this;
    }

    cityFieldContains(value) {
        this.getCityField().should('have.value', value);
        return this;
    }

    clearFields() {
        this.getNameField().clear();
        this.getCityField().clear();

        return this;
    }

    // returns fields
    getSubmitButton = () => cy.get(this.$$.submitButton);
    getClearButton = () => cy.get(this.$$.clearButton);
    getNameField = () => cy.get(this.$$.fields.name);
    getCityField = () => cy.get(this.$$.fields.city);
}

export default SearchComponent
