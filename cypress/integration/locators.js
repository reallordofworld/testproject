module.exports = {
    board: {
        container: '.App-container',
        memberName: '.CrewMember-container .CrewMemeber-name > div:first-child',
    },
    search: {
        container: '#filters',
        submitButton: '#filters [type="submit"]',
        clearButton: '#filters [type="button"]',
        fields: {
            city: '#filters #city',
            name: '#filters #name',
        },
    },
};
