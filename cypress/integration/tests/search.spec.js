import Search from '../components/Search'
import Board from '../components/Board'

const search = new Search();
const board = new Board();

describe('Search', () => {
    before(() => cy.visit('http://localhost:5000/'));

    context('Valid "name" entered', () => {
        it('When: name field is "emma"', () => {
            search
                .enterName('emma')
                .nameFieldContains('emma')
        });

        it('When: Submit button is clicked', () => {
            search.clickSubmit()
        });

        it('Then: On the board "emma stewart" presents', () => {
            board.isMemberExists('emma stewart');
        });

        after(() => {
            search.clearFields();
        });
    });

    context('Valid "city" entered', () => {
        it('When: city field is "worcester"', () => {
            search
                .enterCity('worcester')
                .cityFieldContains('worcester')
        });

        it('When: Submit button is clicked', () => {
            search.clickSubmit()
        });

        it('Then: First member on the board is "emma stewart"', () => {
            board.isMemberExists('emma stewart');
        });

        after(() => {
            search.clearFields();
        });
    });

    context('Valid "name" and "city" entered', () => {
        it('When: name field is "emma"', () => {
            search
                .enterName('emma')
                .nameFieldContains('emma')
        });

        it('When: city field is "worcester"', () => {
            search
                .enterCity('worcester')
                .cityFieldContains('worcester')
        });

        it('When: Submit button is clicked', () => {
            search.clickSubmit()
        });

        it('Then: First member on the board is "emma stewart"', () => {
            board.isMemberExists('emma stewart');
        });

        after(() => {
            search.clearFields();
        });
    });

    context('Valid "name" and invalid "city" entered', () => {
        it('When: name field is "emma"', () => {
            search
                .enterName('emma')
                .nameFieldContains('emma')
        });

        it('When: city field is "kiev"', () => {
            search
                .enterCity('kiev')
                .cityFieldContains('kiev')
        });

        it('When: Submit button is clicked', () => {
            search.clickSubmit()
        });

        it('Then: Board doesn\'t present members', () => {
            board.isMemberNotExist('emma stewart');
        });

        after(() => {
            search.clearFields();
        });
    });

    context('Invalid "name" and valid "city" entered', () => {
        it('When: name field is "ema"', () => {
            search
                .enterName('ema')
                .nameFieldContains('ema')
        });

        it('When: city field is "worcester"', () => {
            search
                .enterCity('worcester')
                .cityFieldContains('worcester')
        });

        it('When: Submit button is clicked', () => {
            search.clickSubmit()
        });

        it('Then: First member on the board is "emma stewart"', () => {
            board.isMemberNotExist('emma stewart');
        });

        after(() => {
            search.clearFields();
        });
    });

    context('"Clear" button reset filters for board', () => {
        it('When: name field is "emma"', () => {
            search
                .enterName('emma')
                .nameFieldContains('emma')
        });

        it('When: Submit button is clicked', () => {
            search.clickSubmit()
        });

        it('Then: First member on the board is "emma stewart"', () => {
            board.isMemberExists('emma stewart');
        });

        it('When: Clear button is clicked', () => {
            search.clickClear()
        });

        it('Then: First member on the board is "lloyd gonzalez"', () => {
            board.isMemberExists('lloyd gonzalez');
        });

        after(() => {
            search.clearFields();
        });
    });

    context('"Clear" button reset search fields', () => {
        it('When: name field is "emma"', () => {
            search
                .enterName('emma')
                .nameFieldContains('emma')
        });

        it('When: name field is "ema"', () => {
            search
                .enterCity('worcester')
                .cityFieldContains('worcester')
        });

        it('When: Clear button is clicked', () => {
            search.clickClear()
        });

        it('Then: name and city fields are empty', () => {
            search
                .nameFieldContains('')
                .cityFieldContains('')
        });

        after(() => {
            search.clearFields();
        });
    })
});
